#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$DIR/docroot"
find . -iname ".git" | sed -E "s:^\./(.+)/\.git$:cd \1;echo \"========== \1\";git status;cd \"$DIR/docroot\":" | bash

read -p "Rebuild codebase? (y/n) " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
    echo "=Yes"
    echo "Re-making codebase..."
    cd $DIR
    if [ -d "$DIR/docroot" ]; then
        if [ -L "$DIR/docroot" ]; then
            echo "docroot/ cannot be a symbolic link!";
            exit 1;
        fi
        echo "(to use --no-cache, first delete the docroot directory)"
        chmod -R 777 "$DIR/docroot" &&
        rm -rf "$DIR/docroot" &&
        drush make --working-copy --no-gitinfofile makefiles/incubator.dev docroot
    else
        drush make --working-copy --no-gitinfofile --no-cache makefiles/incubator.dev docroot
    fi
fi

read -p "Initialize site? (y/n)" -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
    drush si bahai_incubator --db-url="mysql://drupal7:drupal7@database:3306/drupal7" --db-su-pw="drupal7" -y
    drush en simpletest incubator_testing -y
    drush pm-disable distro_update -y
fi
